#!/bin/zsh

#input="interface.txt"
#interface= read line


input="interface.txt"
while IFS= read -u 3 -r line
do
  echo "$line"
  # get ip address of known host
  sudo ifdown $line && sudo ifdown wlan1 && sudo ifup wlan1
  sudo ifup $line
# use file descriptor 3 for ssh else it does not work
done 3<$input

#interface='wlx00e02d424561'
#sudo ifdown $interface && sudo ifdown wlan1 && sudo ifup wlan1
#sudo ifup $interface
