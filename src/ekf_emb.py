#!/usr/bin/env python
import rospy
import numpy as np
from telloz.msg import range_n_bearing_sensor
from telloz.msg import virtual_sensor
from telloz.msg import tracked_robot
from telloz.msg import ekf_sensor
from geometry_msgs.msg import Twist, Vector3, Pose2D
from std_msgs.msg import Float32, String, Empty
import math
from filterpy.kalman import ExtendedKalmanFilter
import time

class Tracker(object):
    """
    It tracks robots which is detected from range and bearing. Every robot information is stored in Tracked_Object class. It has a list which is store this class. If there is no information about robot for ten times search process
    it is get rid of these object. If there is information which is not related with tracked robots. It creates new tracked objects.
    """
    
    def __init__(self):
        self.get_params()
        
        # Initiliaze some necessary variables
        self.tracked_object_list=[]
        self.range_n_bearing_vals = []
        self.unique_id = 0
        
        # Get namespace 
        namespace = rospy.get_namespace()
        
        # Subscribe necessary topics
        rospy.Subscriber(namespace + "sensors/range_n_bearing",
                        range_n_bearing_sensor,
                        self.rb_callback,
                        queue_size=1)        
        
        
        self.ekf_publisher = rospy.Publisher(namespace + "sensors/ekf",
                                             ekf_sensor,
                                             queue_size=1)
        

    def get_params(self):
        """
        Parameters can come from parameter sever.
        """
        self.angle_difference = np.pi/6
        
        if rospy.has_param('/ekf_params'):
            params = rospy.get_param('/ekf_params')
            self.kobot_radius = params['kobot_radius']
            self.track_norm = params['track_norm']
            self.ekf_range_limit = params['ekf_range_limit']
        
        else:
            self.kobot_radius = 10500
            self.track_norm = 10500
            self.ekf_range_limit = 3000
        

    def get_range_and_bearing_points(self, sigma_list):
        
        robot_point_list = []
        
        # Check every sensor measurement which is related robot, to append robot_point_list.
        for i,sigma in enumerate(sigma_list):
            if sigma.is_robot and sigma.range < self.ekf_range_limit:
                point = np.array([sigma.range, wrap2pi(i*self.angle_difference)]) # [range, theta[rad]]
                robot_point_list.append(point)
        
        return robot_point_list
                

    def rb_callback(self, data):
        
        # Getting range and bearing data
        self.range_n_bearing_vals = data.range_n_bearing
        
        # Find points which is related with potential robots
        self.range_n_bearing_points = self.get_range_and_bearing_points(self.range_n_bearing_vals)
        
        # Associate related points with correct robot
        self.robot_points = self.get_robot_points(self.range_n_bearing_points)

        # Update tracked objects with related robot points
        self.tracked_object_list = self.update(self.tracked_object_list, self.robot_points)
        
        # Print tracked object number   
        #rospy.logwarn("Tracked object number: {}".format(len(self.tracked_object_list)))
        
        # Publish tracked objects
        self.publish_tracked_objects()
        
    
    def publish_tracked_objects(self):
        
        # Create ekf_sensor message
        ekf_sensor_msg = ekf_sensor()
        
        # Publish tracked objects with tracked_robot message
        for tracked_object in self.tracked_object_list:
            tracked_robot_msg = tracked_robot()
            tracked_robot_msg.is_tracked = True
            tracked_robot_msg.id = tracked_object.id
            tracked_robot_msg.twist.linear.x = tracked_object.ekf_pose[0]
            tracked_robot_msg.twist.linear.y = tracked_object.ekf_pose[1]
            tracked_robot_msg.twist.angular.x = tracked_object.ekf_vel[0]
            tracked_robot_msg.twist.angular.y = tracked_object.ekf_vel[1]
            
            # Append tracked_robot message to ekf_sensor message
            ekf_sensor_msg.tracked_robots.append(tracked_robot_msg)
            
        # Publist tracked robot message
        self.ekf_publisher.publish(ekf_sensor_msg)
            
        
    def is_relevant(self, robot_1, robot_2):
        
        angle_diff = robot_2[1]-robot_1[1]
        a = robot_1[0]
        b = robot_2[0]
        
        # Let's use cosine theorem to calculate distance between robots.
        
        distance = np.sqrt(a**2 + b**2 - 2*a*b*np.cos(angle_diff))
        
        if distance <= self.kobot_radius:
            average_range = (a+b)/2
            average_angle = robot_1[0] + angle_diff/2
            return True, np.array([average_range, average_angle])
        
        else:
            return False, None
        
    
    def get_robot_points(self, range_and_bearing_points):
        
        robot_points = []
        rnb_points = range_and_bearing_points.copy()
        
        # Check distance between range and bearing points.
        for i in range(len(rnb_points)):
            for j in range(len(rnb_points)):
                
                if i == j:
                    continue
                
                elif j <= i:
                    continue
                
                else:
                    # If point1 and point2 related with same robot, we can use average of these two point to update position of tracked object
                    is_relevant, new_point = self.is_relevant(rnb_points[i], rnb_points[j])
                    
                    # If there is new point, it can be added to robot_points list.
                    if is_relevant:
                        robot_points.append(new_point)
                        range_and_bearing_points.remove(rnb_points[i])
                        range_and_bearing_points.remove(rnb_points[j])
                        break
        
        # After we remove all average robot points, we can use range_and_bearing points that remain        
        for point in range_and_bearing_points:
            robot_points.append(point)
            
        return robot_points            
    
    
    def update(self, object_list, robot_point_list):
        
        # There are robot points which is obtained from range_and_bearing
        # They have to classified like tracked or untracked.
        # If they tracked, they have to use for estimation by correct tracked_object.
        # If they don't track, they have to be track by new tracked_objects.
        tracked_robot_points = []
        untracked_robot_point = []
        untracked_object_list = object_list.copy()
        

        for robot_point in robot_point_list:
            # Assume that point is related with untracked object
            tracked_point_status = False
            
            # Loop in tracked object list
            for object in object_list:
                
                # Check whether tracked object and robot point are related each other or not
                if self.is_close(robot_point, object.instant_estimated_polar_position):
                    
                    # Object and point are related with each other so tracked point status equals to True
                    tracked_point_status = True
                    
                    # This point can be used to estimate new position of tracked object. Their dismiss counter is going to be zero.
                    object.estimate(robot_point)
                    
                    # This point is appended to list that related with tracked robots
                    tracked_robot_points.append(robot_point)
                    
                    break
            
            # If robot point did not match with any tracked object, this point can be added to untracked robot point list    
            if tracked_point_status == False:
                untracked_robot_point.append(robot_point)
                
        # Create new object to track point that is not related with any tracked object
        for new_object_point in untracked_robot_point:
            # New object created and added to object list
            new_object = Tracked_object(new_object_point, self.unique_id)
            self.unique_id += 1
            object_list.append(new_object)
            
        # Loop object in untracked_object_list
        for object in untracked_object_list:    
            # This function increse dismiss counter of all object in this list. If they are tracked objects, their dismiss counter is going to be zero next iteration.
            # If they are not tracked object, their dismiss counter increase to dismiss limit. After that they are removed from tracked object list. 
            if object.check_dismiss_counter():
                object_list.remove(object)
        
        return object_list
    
    def is_close(self, point_1, point_2):
        """
        Function measures closeness of robot point and estimated position of tracked object.
        """
        
        angle_diff = point_2[1] - point_1[1]
        
        a = point_1[0]
        b = point_2[0]
        
        distance = np.sqrt(a**2 + b**2 - 2*a*b*np.cos(angle_diff))
        
        # rospy.logerr("Distance: {}".format(distance))1
        
        if distance < self.track_norm:
            return True
        
        else:
            return False
        
class Tracked_object(object):
    
    def __init__(self, initiliaze_position,id):
        # Initilize tracked object to use in tracking algorithm
        self.history = []   # History of tracked object
        self.id = id    # Unique id of tracked object
        # self.global_velocity = None
        self.initiliaze_positions = initiliaze_position  # Initial position of tracked object
        self.old_pose_x, self.old_pose_y = None, None
        self.ekf_pose = np.array([initiliaze_position[0]*np.cos(initiliaze_position[1]), initiliaze_position[0]*np.sin(initiliaze_position[1])])  # Initial position of tracked object in cartesian coordinate
        self.ekf_vel = np.array([0, 0]) # Initial velocity of tracked object
        self.instant_estimated_polar_position = initiliaze_position  # Instant estimated position of tracked object but firstly is equal to initial position
        self.dt = 1/15
        self.get_params()
        self.dismiss_counter = 0
        self.initiliaze_EKF()

    def get_params(self):
        # Get parameters from parameter server
        if rospy.has_param('/ekf_params'):
            params = rospy.get_param('/ekf_params')
            self.EKF = params['ekf_status']
            self.dismiss_limit = params['dismiss_limit']
            self.init_limit = params['init_limit']
        else:
            self.EKF = True
            self.dismiss_limit = 5
            self.init_limit = 3
    
    
    def initiliaze_EKF(self):
        # Initiliaze EKF
        # Kalman iniitialization counter provides to wait for first estimation
        self.kalman_counter = 0
        # Extended Kalman Filter
        self.rk = ExtendedKalmanFilter(dim_x=4, dim_z=2) 
        # self.rk.x = np.array([self.initiliaze_positions[0], self.initiliaze_positions[1], 20, 20])
        self.rk.F = np.eye(4) + np.array([[0, 0, 1, 0],
                                          [0, 0, 0, 1],
                                          [0, 0, 0, 0],
                                          [0, 0, 0, 0]]) * self.dt
        
        self.rk.P = np.array([[8000, 0, 0, 0],
                              [0, 8000, 0, 0],
                              [0, 0, 1000, 0],
                              [0, 0, 0, 1000]])
        
        self.rk.Q = np.array([[64, 0, 0, 0],
                              [0, 64, 0, 0],
                              [0, 0, 40, 0],
                              [0, 0, 0, 40]])
        
        self.rk.R = np.array([[(15.0)**2, 0.0], 
                              [0.0, (np.deg2rad(8.0))**2]])
        
        # print(self.rk.R)
    
    def HJacobian_at(self, x):
        """
        H is computed in this function.
        """
        _x = x[0]
        _y = x[1]
        r = np.sqrt(x[0]**2 + x[1]**2)
        r2 = x[0]**2 + x[1]**2
        
        H = np.array([[_x/r, _y/r, 0, 0],
                      [-_y/r2, _x/r2, 0, 0]])
        
        return H
    
    def hx(self, x):
        """
        It returns to our needs
        """
        
        range = np.sqrt(x[0]**2 + x[1]**2)
        theta = np.arctan2(x[1],x[0])
        # if theta < 0:
        #     theta = theta + np.pi
        return np.array([range, theta])
    
    def check_dismiss_counter(self):
        """
        Increases dismiss counter.
        """
        
        self.dismiss_counter = self.dismiss_counter + 1
        
        if self.dismiss_counter >= self.dismiss_limit:
            return True
        
        else:
            return False
    
    def estimate(self, related_point = None):
        """
        Estimates positions with data comes from Tracker.
        """
        self.measured_polar_pose = related_point
        # Estimate using EKF
        self.instant_estimated_position = self.estimate_w_ekf(self.measured_polar_pose)
        
        # All tracked object's positions are stored in history
        self.history.append(self.instant_estimated_position)
        
        # Tranform cartesian position to polar position
        self.instant_estimated_polar_position = self.cartesian2polar(self.instant_estimated_position)
        
        # Object is still being tracked so dismiss counter is reset
        self.dismiss_counter = 0
            
    
    def cartesian2polar(self, pose):
        
        range_of_vec = np.sqrt(pose[0]**2 + pose[1]**2)
        alpha_of_vec = np.arctan2(pose[1],pose[0])
        polar_pose = np.array([range_of_vec, alpha_of_vec])
        return polar_pose    
        
        
    def estimate_w_ekf(self, measured_position):
        """
        It uses ekf to estimate pose.
        """
        # Prepare EKF inputs for estimation
        self.z = measured_position  # Polar position
        self.pos_x = measured_position[0]*np.cos(measured_position[1]) # Cartesian position
        self.pos_y = measured_position[0]*np.sin(measured_position[1]) # Cartesian position
        
        # If kalman counter is smaller than init_limit, parameters have to manipulate for first estimation.
        if self.kalman_counter < self.init_limit:
            # Store first three measurements for initilization velocity estimation
            if self.old_pose_x == None:
                self.old_pose_x = self.pos_x
                self.old_pose_y = self.pos_y
                self._vel = []
                
            else:
                # Calculate velocity and append to list
                vel_x = (self.pos_x - self.old_pose_x)/self.dt
                vel_y = (self.pos_y - self.old_pose_y)/self.dt
                self._vel.append(np.array([vel_x, vel_y]))   
                
                # Update old pose
                self.old_pose_x = self.pos_x
                self.old_pose_y = self.pos_y            
            
            # Update ekf_pose, if we dont't have any measurement, ekf_pose is the last measured position
            self.ekf_pose = np.array([self.pos_x, self.pos_y])
        
        elif self.kalman_counter == self.init_limit:
            
            # Calculate velocity and append to list
            vel_x = (self.pos_x - self.old_pose_x)/self.dt
            vel_y = (self.pos_y - self.old_pose_y)/self.dt
            self._vel.append(np.array([vel_x, vel_y]))  
            
            # Calculate average velocity
            total_vel_x, total_vel_y = 0, 0
            for _x, _y in self._vel:
                total_vel_x += _x
                total_vel_y += _y
            self.vel_x = total_vel_x/len(self._vel)
            self.vel_y = total_vel_y/len(self._vel)
            
            # EKF input for first estimation
            self.rk.x = np.array([self.pos_x, self.pos_y, self.vel_x, self.vel_y]) # Initiliaze EKF
            
            # Update ekf_pose, if we dont't have any measurement, ekf_pose is the last measured position
            self.ekf_pose = np.array([self.pos_x, self.pos_y])

            
        else:
            # Update all EKF parameters            
            self.rk.update(self.z, self.HJacobian_at, self.hx)
            
            # Make prediction for next step
            self.rk.predict()
            
            # Get estimated pose and velocity
            self.ekf_pose = np.array([self.rk.x[0], self.rk.x[1]])
            self.ekf_vel = np.array([self.rk.x[2], self.rk.x[3]])

        # Increase kalman counter
        self.kalman_counter += 1 
           
        return self.ekf_pose


def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*np.pi)
    if ang > np.pi:
        ang = np.pi - ang
        return -(np.pi + ang)
    elif ang < np.pi:
        return ang
    else:
        return -np.pi    

def rotate_w_radian(vec_1, radian):
    
    x = vec_1[0]
    y = vec_1[1]
    rotated_x = x * np.cos(radian) - np.sin(radian)
    rotated_y = x * np.sin(radian) + np.cos(radian)
    
    return np.array([rotated_x, rotated_y])
    

def start():
    # Node initialization
    rospy.init_node("EKF")
    
    # Create a Tracker to track objects
    tracker = Tracker()
    rospy.spin()

# start from command-line
if __name__ == '__main__':
    start()
