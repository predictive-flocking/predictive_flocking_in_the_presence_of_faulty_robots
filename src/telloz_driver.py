#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from std_msgs.msg import Empty, Bool, String, Int8, Int16,Float32,UInt8
from math import pi
from tello import Tello
import math
import os
class Telloz(Tello):
    """
    Responsible for communicating with the Tello through the 
    Python SDK
    """
    def __init__(self, Tello):
        self.hostname = os.environ['ROS_HOSTNAME']
        self.drone = Tello()
        self.desired_vel_y = 0
        self.desired_vel_x = 0
        self.desired_vel_z = 0
        self.desired_ang_vel = 0
        self.control_vel_z = 0
        self.heading_offset_value = 0
        self.diff_prev = 0
        self.z_control_mode = False
        self.MAX_STICK_INPUT = 40
        self.drone.connect()
        # subs. for global topics (common for all tellos)
        self.sub_takeoff = rospy.Subscriber('/tello/takeoff', Empty, self.cb_takeoff)
        self.sub_land = rospy.Subscriber('/tello/land', Empty, self.cb_land)
        self.sub_z_control = rospy.Subscriber('/tello/z_ctrl', Bool, self.cb_z_ctrl_mode)
        self.sub_heading_reset = rospy.Subscriber('/tello/heading_reset',Empty,self.cb_heading_reset)
        # subs. for namespace topics (seperate for all tellos)
        self.sub_cmd_vel = rospy.Subscriber('cmd_vel', Twist, self.cb_cmd_vel)

        self.heading_offset_pub = rospy.Publisher('heading_offset_degree',Int16,queue_size=1)
        self.heading_pub = rospy.Publisher('sensors/heading',Float32,queue_size=1)
        self.height_pub = rospy.Publisher('sensors/height',Float32,queue_size=1)
        self.battery_pub = rospy.Publisher('battery',UInt8,queue_size=1)
        self.z_speed_pub = rospy.Publisher('sensors/z_speed',Float32,queue_size=1)

    def get_params(self):
        """
        params. are checked constantly and are
        updated 
        """
        if rospy.has_param('/telloz_driver_params'):
            # fetch a group (dictionary) of parameters
            params = rospy.get_param('/telloz_driver_params')
            self.Kp_z = params['Kp_z']
            self.Kd_z = params['Kd_z']
            self.desired_z = params['z_ref']

        else: 
            rospy.logerr("No Params")
            # feed default vals
            self.Kp_z = 3
            self.Kd_z = 0.5
            self.desired_z = 100

    def cb_cmd_vel(self, msg):
        self.desired_vel_x = int(msg.linear.y*10)
        self.desired_vel_y = int(msg.linear.x*10)
        self.desired_vel_z = int(msg.linear.z*10)
        self.desired_ang_vel = int(-msg.angular.z*10)
    
    def cb_takeoff(self, msg):
        self.drone.takeoff()

    def cb_land(self, msg):
        self.drone.land()
    
    def cb_heading_reset(self, msg):
        self.heading_offset_value = self.drone.get_yaw()
        self.heading_offset_pub.publish(self.heading_offset_value)
    
    def cb_z_ctrl_mode(self,msg):
        self.z_control_mode = msg.data

    def get_heading(self):
        drone_yaw = self.drone.get_yaw()
        # calculate desired heading degree
        heading_degree = drone_yaw - self.heading_offset_value
        # convert to rads, constrain to +pi, -pi and correct the direction
        heading = -wrap2pi(heading_degree * math.pi/180.0)
        self.heading_pub.publish(heading)

    def get_height(self):
        self.height = self.drone.get_distance_tof()
        self.height_pub.publish(self.height)


    def control_height(self,freq):
        time_diff = 1/freq
        
        self.diff = self.desired_z - self.height
        derivative_of_err = (self.diff-self.diff_prev)/time_diff
        self.z_speed_pub.publish(derivative_of_err)
        if self.MAX_STICK_INPUT/self.Kp_z >= abs(self.diff):
            self.control_vel_z = int(self.Kp_z * self.diff + self.Kd_z * derivative_of_err)
        else:
            if self.diff > 0:
                self.control_vel_z = int(self.MAX_STICK_INPUT)
            else:
                self.control_vel_z = int(-self.MAX_STICK_INPUT)

        self.diff_prev = self.diff

    def rc_command_generator(self):

        if self.z_control_mode:
            vel_z = self.control_vel_z
        else:
             vel_z = self.desired_vel_z
        self.drone.send_rc_control(self.desired_vel_x,self.desired_vel_y,vel_z,self.desired_ang_vel)

    def get_battery(self):
        battery_msg = UInt8()
        battery_msg = self.drone.get_battery()
        self.battery_pub.publish(battery_msg)

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*math.pi)
    if ang > math.pi:
        ang = math.pi - ang
        return -(math.pi + ang)
    else:
        return ang

def start():
    """
    this ROS Node converts teleop commands
    into mapped motor commands for robot
    """
    rospy.init_node("telloz_driver")
    telloz = Telloz(Tello)
    freq = 20
    cntr = 0
    rate = rospy.Rate(freq)
    telloz.get_params()
    while not rospy.is_shutdown():
        if cntr == 40:
            # at 0.2 Hz
            telloz.get_params()
            telloz.get_battery()
            cntr = 0
        telloz.get_heading()
        telloz.get_height()
        telloz.control_height(freq)
        telloz.rc_command_generator()
        cntr += 1
        rate.sleep()
        
# start from command-line
if __name__ == '__main__':
    start()
