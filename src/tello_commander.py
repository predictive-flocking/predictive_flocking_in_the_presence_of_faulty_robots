#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from std_msgs.msg import Float64, String, Int8
from math import pi
import os


class Telloz(object):
    """
    Responsible for converting 2-D vehicle twist
    to seperate motor velocity control commands
    """

    def __init__(self):

        self.HOSTNAME = os.environ['ROS_HOSTNAME']
        self.commander = ''

        self.th_dot = 0
        rospy.Subscriber(
            "/key_vel",
            Twist,
            self.callback_key_vel)

        rospy.Subscriber(
            "/commander",
            String,
            self.callback_commander)

        rospy.Subscriber(
            "nav_vel",
            Twist,
            self.callback_nav_vel)

        self.cmd_vel_pub = rospy.Publisher(
            "cmd_vel",
            Twist,
            queue_size=1)

    def callback_key_vel(self, data):
        """
        Only if commander is 'key'
        feed to cmd_vel callback
        else do nothing
        """
        if self.commander == 'key':
            self.cmd_vel_pub.publish(data)

    def callback_nav_vel(self, data):
        """
        Only if commander is 'nav'
        feed to cmd_vel callback
        else do nothing
        """
        if self.commander == 'nav':
            self.cmd_vel_pub.publish(data)

    def callback_commander(self, data):
        """
        Callback for commander msg. which
        decides which vel is fed into cmd_vel
        """
        # correct way of parsing string from msg.
        self.commander_type = String()
        self.commander_type = data.data
        if self.commander_type == 'key':
            # all robots get cmd_vel from key_teleop
            self.commander = 'key'
        elif self.commander_type == 'nav':
            # all robots get cmd_vel from nav_msg
            # i.e. flocking drives the robots
            self.commander = 'nav'
        elif self.commander_type == 'key'+self.HOSTNAME:
            # only the robot with matching hostname
            # is driven by key_teleop
            self.commander = 'key'
        else:
            self.commander = 'nav'


def start():
    """
    this ROS Node converts teleop commands
    into mapped motor commands for robot
    """
    rospy.init_node("teleop_key")
    telloz = Telloz()
    rospy.spin()


# start from command-line
if __name__ == '__main__':
    start()
