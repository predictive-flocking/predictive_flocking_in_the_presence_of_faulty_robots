#!/usr/bin/env python
from re import I
import time
from telloz.msg import range_n_bearing_sensor
from telloz.msg import virtual_sensor
from telloz.msg import ekf_sensor
import rospy
from geometry_msgs.msg import Twist, Vector3
from std_msgs.msg import Float32, String, Bool
import numpy as np
import math
from numpy import linalg

class AvoidanceDriver(object):

    def __init__(self):
        self.get_params()
        #define the variables
        self.virtual_heading_vector_list = []
        self.range_n_bearing_vals = [] #list for collecting time-of-flight sensor readings
        self.robot_pos_list = np.array([]) #list for collecting Kalman position estimations of the neighbors
        self.robot_vel_list = np.array([]) #list for collecting Kalman velocity estimations of the neighbors
        self.heading_vector = [1, 0] #physical heading vector of the robot
        self.heading = 0 #physical heading angle of the robot
        self.speed = 0 #speed of the robot
        self.heading_v = 0 #virtual heading angle of the robot based on velocity
        self.heading_vector_v = np.array([1,0,0]) #virtual heading vector of the robot based on velocity, direction of motion
        self.velocity_vec = np.array([0,0,0]) #velocity of the robot
        self.fault_state = False #fault state of the robot


        rospy.Subscriber("sensors/range_n_bearing",
                        range_n_bearing_sensor,
                        self.rb_callback,
                        queue_size=1)
        rospy.Subscriber("sensors/heading",
                        Float32,
                        self.heading_callback,
                        queue_size=1)

        rospy.Subscriber("sensors/virtual_heading",
                        virtual_sensor,
                        self.virtual_heading_callback,
                        queue_size=1)
        
        rospy.Subscriber("fault_state",
                         Bool,
                         self.fault_state_callback,
                         queue_size=1)

        self.nav_vel_publisher = rospy.Publisher(
            "nav_vel",
            Twist, queue_size=1)
        self.ch_publisher = rospy.Publisher(
            "ch_vector",
            Vector3, queue_size=1)
        
        self.vh_publisher = rospy.Publisher(
            "vh_vector",
            Vector3, queue_size=1)
        
        self.vf_publisher = rospy.Publisher(
            "vf_vector",
            Vector3, queue_size=1)
        
        self.dh_publisher = rospy.Publisher(
            "dh_vector",
            Vector3, queue_size=1)
        
        #Create a subscriber to read EKF data
        rospy.Subscriber("sensors/ekf",
                         ekf_sensor,
                         self.ekf_callback,
                         queue_size=1)
        
    def fault_state_callback(self, msg):
        self.fault_state = msg.data
    
        
    def ekf_callback(self, msg):
        '''
        It returns the positions and velocities of the neighbors
        '''
        
        #Create empty lists to store the positions and velocities of the neighbors
        self.robot_pos_list = []
        self.robot_vel_list = []
        
        #Loop through the EKF message of the neighbors
        for tracked_robot in msg.tracked_robots:
            #Get the position and velocity of the neighbor
            self.robot_pos_list.append([tracked_robot.twist.linear.x, tracked_robot.twist.linear.y, 0]) #neighbors estimated relative positions, [mm]
            self.robot_vel_list.append([tracked_robot.twist.angular.x, tracked_robot.twist.angular.y, 0]) #neighbors estimated relative velocities, [mm]
            
        self.robot_pos_list = np.array(self.robot_pos_list)/1000 #neighbors estimated relative positions, [m]
        self.robot_vel_list = np.array(self.robot_vel_list)/1000 #neighbors estimated relative velocities, [m]
        
        return self.robot_pos_list, self.robot_vel_list

    ## Simulator settings
    '''
    def get_params(self):
        #Flocking parameters:
        self.dt = 0.1 #discrete control step, [s]
        self.head_search_num = 9 #number of predicted headings at one level of the search tree
        self.speed_search_num = 9 #number of predicted speeds at one level of the search tree
        self.depth = 2 #depth of the search tree
        self.beam_width = 2 #beam-width for the beam search
        self.quad_rad = 0.10 #radius of the robot, [m]
        self.d_ra = 1.0 #inter-robot reference distance, [m]
        self.d_rr = 0.7 #inter-robot avoidance threshold, [m]
        self.mig_ang = 0 #migration direction angle, [rad]
        self.mig_spd = 0.3 #migration speed, [m/s]
        self.mig_vel = self.mig_spd*np.array([np.cos(self.mig_ang), np.sin(self.mig_ang)]) #migration velocity, [m/s]
        self.max_spd = 0.5 #max. speed, [m/s]
        self.bias_spd = 0.05 #min. speed, [m/s]
        self.sense_range = 1.8 #sensing radius, [m]
        self.delta_a = 0.075 #search step for the heading
        self.delta_b = 0.025 #search step for the speed
        self.k_ra = 20 #inter-robot distance heuristic gain
        self.k_rr = 15 #inter-robot avoidance heuristic gain
        self.k_o = 2.5 #obstacle-avoidance heuristic gain
        self.d_0 = 1 #obstacle avoidance threshold
        self.d_safe = 0.1 #safety distance for collision avoidance, [m]
        self.k_s = 15 #speed heuristic gain
        self.k_d = 4.5 #direction heuristic gain
        self.num_sensors = 12 #number of time-of-flight sensors on the range and bearing system
        '''
        
    ## Real robot settings
    def get_params(self):
        #Flocking parameters:
        self.dt = 0.1 #discrete control step, [s]
        self.head_search_num = 5 #number of predicted headings at one level of the search tree
        self.speed_search_num = 5 #number of predicted speeds at one level of the search tree
        self.depth = 2 #depth of the search tree
        self.beam_width = 2 #beam-width for the beam search
        self.quad_rad = 0.10 #radius of the robot, [m]
        self.d_ra = 1.0 #inter-robot reference distance, [m]
        self.d_rr = 0.7 #inter-robot avoidance threshold, [m]
        self.mig_ang = 0 #migration direction angle, [rad]
        self.mig_spd = 0.3 #migration speed, [m/s]
        self.mig_vel = self.mig_spd*np.array([np.cos(self.mig_ang), np.sin(self.mig_ang)]) #migration velocity, [m/s]
        self.max_spd = 0.5 #max. speed, [m/s]
        self.bias_spd = 0.05 #min. speed, [m/s]
        self.sense_range = 1.8 #sensing radius, [m]
        self.delta_a = 0.15 #search step for the heading
        self.delta_b = 0.05 #search step for the speed
        self.k_ra = 10 #inter-robot distance heuristic gain
        self.k_rr = 15 #inter-robot avoidance heuristic gain
        self.k_o = 3.0 #obstacle-avoidance heuristic gain
        self.d_0 = 1 #obstacle avoidance threshold
        self.d_safe = 0.1 #safety distance for collision avoidance, [m]
        self.k_s = 15 #speed heuristic gain
        self.k_d = 4.5 #direction heuristic gain
        self.num_sensors = 12 #number of time-of-flight sensors on the range and bearing system

    def heading_search_list(self, currentHead):
        #generate possible future headings at next time step
        searchNum = self.head_search_num
        searchNumN = self.delta_a*(np.floor(searchNum/2))
        headingSearchList = np.zeros((searchNum,1))+currentHead
        headingSearchList = headingSearchList + np.linspace(-searchNumN,searchNumN,searchNum).reshape((-1, 1))
        return headingSearchList

    def speed_search_list(self, currentVel):
        #generate possible future speeds at next time step
        searchNumSpd = self.speed_search_num
        searchNumN = 0.1*(np.floor(searchNumSpd/2))
        velocitySearchList = np.zeros((searchNumSpd,1))+currentVel
        velocitySearchList = velocitySearchList + np.linspace(-searchNumN,searchNumN,searchNumSpd).reshape((-1, 1))
        velocitySearchList[velocitySearchList<self.bias_spd] = self.bias_spd
        velocitySearchList[velocitySearchList>self.max_spd] = self.max_spd
        return velocitySearchList
    
    def quad_dist_heur(self, quadRelMagList, quadVelList,quadKK):
    #heuristic calculation for quad-quad collision avoidance and quad proximality
        quadHeurCost = 0.0
        for ddd in range (0,quadKK):
            quadNeigDistRel = quadRelMagList[ddd][0]
            if quadNeigDistRel > self.d_ra:
                neigNormSpd = np.linalg.norm(quadVelList[ddd,:])/self.mig_spd
                quadNeigCostCoeff = self.k_ra*neigNormSpd*np.power(quadNeigDistRel - self.d_ra,2)
            else:
                quadNeigCostCoeff = self.k_ra*np.power(quadNeigDistRel - self.d_ra,2)
            quadHeurCost = quadHeurCost + quadNeigCostCoeff
        return quadHeurCost

    
    def quad_coll_heur(self, quadRelMagList, quadVecList, quadDistMagList, quadVelList, predHeading, quadKK):
    #heuristic calculation for quad-quad collision avoidance and quad proximality
        quadHeurCost = 0.0
        for ddd in range (0,quadKK):
            quadNeigDistRel = quadRelMagList[ddd][0]
            if quadNeigDistRel < self.d_rr:
                quadNeigDistRel = quadNeigDistRel - self.d_safe
                quadNeigVec = quadVecList[ddd,:]/quadDistMagList[ddd]
                a_r = np.dot(predHeading,quadNeigVec) #approach weight
                if a_r < 0: a_r = 0
                quadNeigCostCoeff = a_r*(self.k_rr*np.power(1/quadNeigDistRel-1/self.d_rr,2))
            else:
                quadNeigCostCoeff = 0
            quadHeurCost = quadHeurCost + quadNeigCostCoeff
        return quadHeurCost
    

    def obs_heur(self, obsRelMagList, obsKK):
    #heuristic calculation for obs-quad collision avoidance
        obsHeurCost = 0.0;
        for ddd in range(0,obsKK):
            obsDist = obsRelMagList[ddd][0]
            obsCostCoeff = (self.k_o*0.5*np.power(1/obsDist-1/self.d_0,2))/obsKK
            obsHeurCost =  obsHeurCost + obsCostCoeff
        return obsHeurCost


    def obs_coll_heur(self, obsRelMagList, obsVecList, obsDistMagList, predHeading, obsKK):
    #heuristic calculation for obs-quad collision avoidance
        obsHeurCost = 0.0;
        for ddd in range(0,obsKK):
            obsDist = obsRelMagList[ddd][0] - self.d_safe #add extra safety since when a_o becomes 0, distance to obstacle, obsDist, becomes ineffective
            obsVec = obsVecList[ddd,:]/obsDistMagList[ddd]
            a_o = np.dot(predHeading,obsVec) #approach weight
            if a_o < 0: a_o = 0
            obsCostCoeff = a_o*(self.k_o*np.power(1/obsDist-1/self.d_0,2))
            obsHeurCost =  obsHeurCost + obsCostCoeff
        return obsHeurCost

    def mig_heur(self, predSpd, predHead):
    #heuristic calculation for tracking the migration velocity
        robotVelVec = predSpd*np.array([np.cos(predHead), np.sin(predHead)])
        migHeurCostDir = self.k_d*(1 - np.dot(self.mig_vel,robotVelVec)/(self.mig_spd*predSpd))
        migHeurCostSpd = self.k_s*np.abs(self.mig_spd - predSpd)
        migHeurCost = migHeurCostDir + migHeurCostSpd
        return migHeurCost

    def virtual_heading_callback(self, data):
        """
        Collect all virtual headings in a list
        """
        self.virtual_heading_vector_list = []
        for virtual_heading in data.sensor_list:
            #append headings in rect. coords.
            self.virtual_heading_vector_list.append(
                [math.cos(virtual_heading), math.sin(virtual_heading), 0])

    def heading_callback(self, data):
        """
        Get the heading of the robot and convert it to rect. coors.
        """
        self.heading = data.data
        self.heading_vector = [math.cos(self.heading), math.sin(self.heading)]

    def rb_callback(self, data):
        """
        Collect all range values in a int list
        """
        self.range_n_bearing_vals = data.range_n_bearing

    def neig_obs_pos_calc(self,sigma_list):
        """
        Get relative positions of the neighboring robots
        and obstacles within the sensing range
        """
        rnb_neig_pos_list = []
        rnb_obs_pos_list = []
        sensor_angular_coeff = 2*math.pi / self.num_sensors
        for ind, sigma in enumerate(sigma_list):
            dist = sigma.range/1000
            if dist < self.sense_range:
                if sigma.is_robot:
                    phi = sensor_angular_coeff * ind
                    rnb_neig_pos_list.append([dist*math.cos(phi), dist*math.sin(phi), 0])
                if not sigma.is_robot:
                    phi = sensor_angular_coeff * ind
                    rnb_obs_pos_list.append([dist*math.cos(phi), dist*math.sin(phi), 0])
        return np.asarray(rnb_neig_pos_list), np.asarray(rnb_obs_pos_list)

    def navigate(self):
        """
        Navigate with the Predictive Search Model
        """
        neig_detected_pos_list, obs_detected_pos_list = self.neig_obs_pos_calc(self.range_n_bearing_vals) #get relative positions of the neighbors and obstacles in the sensing range
        neig_detected_pos_list = self.robot_pos_list #get Kalman estimated positions of the neighbors

        quadKK = neig_detected_pos_list.shape[0] #calculate the number of neighbors
        obsKK = obs_detected_pos_list.shape[0] #calculate the number of obstacles
        
        neig_detected = (quadKK > 0)
        obs_detected = (obsKK > 0)

        quad_neig_rel_dist_pos_list = np.zeros((quadKK,3*(self.depth+1))) #create an array for collecting the predicted positions of the neighbors at next time steps
        if neig_detected:
            robot_vel_list_nrel = self.velocity_vec + self.robot_vel_list #transform relative neighbor velocities into non-relative velocities

            quad_neig_rel_dist_pos_list[:,0:3] = neig_detected_pos_list #get current positions of the neighbors
            #predict future positions of the neighbors
            m=1
            for i in range(3,3*self.depth+1,3):
                #quad_neig_rel_dist_pos_list[:,i:i+3] = neig_detected_pos_list + m*self.dt*self.mig_spd*np.hstack((np.cos(self.mig_ang)*np.ones((quadKK,1)), np.sin(self.mig_ang)*np.ones((quadKK,1)), np.zeros((quadKK,1))))
                #self.robot_vel_list
                quad_neig_rel_dist_pos_list[:,i:i+3] = neig_detected_pos_list + m*self.dt*robot_vel_list_nrel
                m=m+1
            quad_pos_list_fstep = quad_neig_rel_dist_pos_list[:,3:6] #predicted positions of other robots for the first step

        #start searching to determine the next velocity input of the robot:
        search_heading_list = self.heading_search_list(self.heading_v) #generate possible future headings at the next time step  
        search_spd_list = self.speed_search_list(self.speed) #generate possible future speeds at the next time step 

        search_arr = np.array([0,0,0,0],dtype=object) #main array that keeps heuristic values, future heading, speed and position states

        for nnn in range(0,self.speed_search_num):
            predicted_spd = search_spd_list[nnn][0] #selected search velocity
            for n in range(0,self.head_search_num):
                predicted_head = search_heading_list[n][0] #selected search heading
                predicted_head_vec = np.array([np.cos(predicted_head), np.sin(predicted_head), 0]) #predicted future heading vector for the selected heading
                predicted_pos = self.dt*predicted_spd*predicted_head_vec #predicted future position vector for the selected heading and speed
                heur = 0
                #inter-robot heuristic calculation:
                if neig_detected:
                    quad_dist_vec_list =  quad_pos_list_fstep - predicted_pos #inter-robot distance vectors
                    quad_dist_mag_list = np.linalg.norm(quad_dist_vec_list, axis=1).reshape((-1, 1)) #inter-robot distances
                    quad_dist_relmag_list = quad_dist_mag_list - self.quad_rad; #inter-robot relative disatances                
                    heur = heur + self.quad_dist_heur(quad_dist_relmag_list,robot_vel_list_nrel,quadKK) #calculate the inter-robot distance heuristic
                    heur = heur + self.quad_coll_heur(quad_dist_relmag_list,quad_dist_vec_list,quad_dist_mag_list,robot_vel_list_nrel,predicted_head_vec,quadKK) #calculate the inter-robot collision heuristic
                
                #quad obstacle heuristic calculation:
                if obs_detected:
                    obs_dist_vec_list =  obs_detected_pos_list - predicted_pos #obstacle-robot distance vectors               
                    obs_dist_mag_list = np.linalg.norm(obs_dist_vec_list, axis=1).reshape((-1, 1)) #obstacle-robot distances
                    obs_dist_relmag_list = obs_dist_mag_list - self.quad_rad #obstacle-robot relative distances
                    heur = heur + self.obs_coll_heur(obs_dist_relmag_list,obs_dist_vec_list,obs_dist_mag_list,predicted_head_vec,obsKK) #calculate obstacle avoidance heuristic and add it to the total heuristic

                #migration heuristic calculation:
                heur = heur + self.mig_heur(predicted_spd,predicted_head) #calculate the migration heuristic and add it to the total heuristic                
                search_arr = np.vstack((search_arr,np.array([heur,predicted_head, predicted_spd, predicted_pos],dtype=object))) #add the search direction with its heuristic value to a list
        search_arr = search_arr[1:,:]  #np.delete(search_arr, (0), axis=0)
        search_arr = search_arr[search_arr[:, 0].argsort()] #sort search directions according to their heuristics
        prev_search_arr = search_arr[0:self.beam_width,:] #select beta (beam-width) number of directions that have lower heuristics

        #continue searching for the next step, for depth = 2
        if neig_detected:
            quad_pos_list_nstep = quad_neig_rel_dist_pos_list[:,6:9]; #predicted positions of other robots for the second step

        search_arr = np.array([0,0,0,0,0,0,0],dtype=object) #main array that keeps heuristic values, future heading, speed and position states

        for ni in range(0,self.beam_width):
            #get states of the parent node to expand it to the next level
            heur_prev = prev_search_arr[ni,0] #heuristic value of the parent node
            head_prev = prev_search_arr[ni,1] #heading of the parent node
            spd_prev = prev_search_arr[ni,2] #speed of the parent node
            pos_prev = prev_search_arr[ni,3:6][0] #position of the parent node

            search_heading_list = self.heading_search_list(head_prev); #generate heading search values
            search_spd_list = self.speed_search_list(spd_prev); #generate velocity search values

            for nnn in range(0,self.speed_search_num):
                predicted_spd = search_spd_list[nnn][0] #selected search velocity

                for n in range(0,self.head_search_num):
                    predicted_head = search_heading_list[n][0] #selected search heading
                    predicted_head_vec = np.array([np.cos(predicted_head), np.sin(predicted_head), 0]) #predicted future heading vector for the selected heading
                    predicted_pos = pos_prev + self.dt*predicted_spd*predicted_head_vec #predicted future position vector for the selected heading and speed
                    heur = heur_prev #add heuristic value of the parent node
                    #quad-quad heuristic calculation:
                    if neig_detected:
                        quad_dist_vec_list =  quad_pos_list_nstep - predicted_pos #inter-robot distance vectors
                        quad_dist_mag_list = np.linalg.norm(quad_dist_vec_list, axis=1).reshape((-1, 1)) #inter-robot distances
                        quad_dist_relmag_list = quad_dist_mag_list - self.quad_rad; #inter-robot relative disatances                
                        heur = heur +self.quad_dist_heur(quad_dist_relmag_list,robot_vel_list_nrel,quadKK) #calculate the inter-robot distance heuristic
                        heur = heur + self.quad_coll_heur(quad_dist_relmag_list,quad_dist_vec_list,quad_dist_mag_list,robot_vel_list_nrel,predicted_head_vec,quadKK) #calculate the inter-robot collision heuristic

                    #quad obstacle heuristic calculation:
                    if obs_detected:
                        obs_dist_vec_list =  obs_detected_pos_list - predicted_pos #obstacle-robot distance vectors               
                        obs_dist_mag_list = np.linalg.norm(obs_dist_vec_list, axis=1).reshape((-1, 1)) #obstacle-robot distances
                        obs_dist_relmag_list = obs_dist_mag_list - self.quad_rad #obstacle-robot relative distances
                        heur = heur + self.obs_coll_heur(obs_dist_relmag_list,obs_dist_vec_list,obs_dist_mag_list,predicted_head_vec,obsKK) #calculate obstacle avoidance heuristic and add it to the total heuristic

                    #migration heuristic calculation:
                    heur = heur + self.mig_heur(predicted_spd,predicted_head) #calculate the migration heuristic and add it to the total heuristic                
                    search_arr = np.vstack((search_arr,np.array([heur,head_prev,spd_prev,pos_prev,predicted_head,predicted_spd,predicted_pos],dtype=object))) #add the search direction with its heuristic value to a list
        
        search_arr = search_arr[1:,:]  #ignore the first row, which is the row of zeros used for initializing the array
        search_arr = search_arr[search_arr[:, 0].argsort()] #sort search directions according to their heuristics

        heading_input = search_arr[0,1]; #select the heading which gives the lowest predicted heuristic
        speed_input = search_arr[0,2]; #select the speed which gives the lowest predicted heuristic
        pos_input =  search_arr[0,3]; #predicted position at the next time step

        u = speed_input #get the next speed input
        u_x = u*math.cos(heading_input) #calculate x-component of the velocity with respect to body-fixed reference frame of the robot
        u_y = u*math.sin(heading_input) #calculate y-component of the velocity with respect to body-fixed reference frame of the robot
        self.heading_v = heading_input #get the next heading angle
        self.heading_vector_v = np.array([np.cos(heading_input), np.sin(heading_input), 0]) #calculate the next heading vector
        self.speed = speed_input #update the speed of the robot
        self.velocity_vec = self.speed*self.heading_vector_v #update the velocity of the robot

        #velocity_command = speed_input*np.array([np.cos(heading_input), np.sin(heading_input)]) #compute the velocity input vector
        #position_command = pos_input

        #if robot is not faulty, it moves with the computed velocity
        if not self.fault_state:
            self.publish_twist(u_x, u_y)
        #if robot is faulty, it hovers staying stationary
        else:
            self.hover()

    #hovers the faulty robot
    def hover(self):
        self.publish_twist(0, 0)

    def visualize(self, a_c, a, h, p):
        """
        Visualize behaviour vectors
        """
        ch_msg = Vector3()
        vh_msg = Vector3()
        vf_msg = Vector3()
        dh_msg = Vector3()
        ch_msg.x, ch_msg.y, ch_msg.z = a_c[0], a_c[1], 0
        vh_msg.x, vh_msg.y, vh_msg.z = h[0], h[1], 0
        vf_msg.x, vf_msg.y, vf_msg.z = p[0], p[1], 0
        dh_msg.x, dh_msg.y, dh_msg.z = a[0], a[1], 0
        self.ch_publisher.publish(ch_msg)
        self.vh_publisher.publish(vh_msg)
        self.vf_publisher.publish(vf_msg)
        self.dh_publisher.publish(dh_msg)

    #publish the velocity command
    def publish_twist(self, x_vel, y_vel):
        """
        Publish ref. vals. for vel. controller
        """
        twist_msg = Twist()
        twist_msg.linear.x = x_vel
        twist_msg.linear.y = y_vel
        #twist_msg.angular.z = theta_vel
        self.nav_vel_publisher.publish(twist_msg)

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*math.pi)
    if ang > math.pi:
        ang = math.pi - ang
        return -(math.pi + ang)
    elif ang < math.pi:
        return ang
    else:
        return -math.pi

def start():
    #For debug add arg to init_mode log_level=rospy.DEBUG
    rospy.init_node("avoidance")
    driver = AvoidanceDriver() #main class for the navigation
    freq = 10 #control frequency, freq=1/dt where dt=0.1 seconds is the control time step
    rate = rospy.Rate(freq)
    malfunctioned = 0
    #while (not rospy.is_shutdown()) and (sim_kk < kk_max):
    while not rospy.is_shutdown():
        driver.navigate() #navigates the robot using Predictive Search Model
        rate.sleep()

#start from command-line
if __name__ == '__main__':
    start()