import sys
import struct
import time
from smbus2 import SMBus


class TOF:
    """
    Each object corresponds to a VL53L1X sensor on the RPS board
    """

    def __init__(self, bus, sensor_id):
        self.bus = bus
        self.sensor_id = sensor_id
        self.rb_distance = 0
        self.distance = 0
        self.is_robot = False
        self.status = 0
        self.ambient = 0
        self.signal = 0
        self.s_r = 0
        self.num_spad = 0

    def __str__(self):
        return "ID : {}, Distance : {}, isRobot : {}, ambient : {}, signal : {}, status : {}, s_r : {}".format(self.sensor_id,
                                                        self.rb_distance,
                                                        self.is_robot,
                                                        self.ambient,
                                                        self.signal,
                                                        self.status,
                                                        self.s_r)

    def parse_rb(self, block):
        """
        Parses 16-bit range and bearing msg to 12-bit distance and 
        1-bit is_robot value
        """
        self.is_robot = False
        MSB = block[0]
        if (MSB & 0x10):
            # 12'thbit of the msg.
            # 4'th bit of the MSB
            # is is_robot value
            self.is_robot = True
            # turn off the 4'th bit
            MSB = MSB & (~0x10)
            block[0] = MSB
        self.rb_distance = struct.unpack("=H", bytes([block[1],block[0]]))[0]
        if (self.rb_distance & 0x8000):
            # MS bit is wrong common i2c error
            # for RPi make it 0
            self.rb_distance = self.rb_distance & (~0x8000)

    def parse_distance(self, block):
        """
        Parses 16-bit range and bearing msg to 12-bit distance and 
        1-bit is_robot value
        """
        self.distance = struct.unpack("=H", chr(block[1])+chr(block[0]))[0]
        if (self.distance & 0x8000):
            # MS bit is wrong common i2c error
            # for RPi make it 0
            self.distance = self.distance & (~0x8000)

    def parse_uint8_t(self, val):
        if (val & 0x80):
            # MS bit is 1 common i2c error
            # for RPi make it 0
            val = val & (~0x80)
        return val


class RPS:
    """
    Corresponds to the Relative Positioning System
    """
    addr = 10

    def __init__(self):
        try:
            self.bus = SMBus(1)
            time.sleep(0.1)
        except IOError:
            print("IO Error on I2C")

        self.tof_arr = []
        self.num_sensors = 12
        self.num_zones = 2
        self.construct_sensor_array()

    def write_ambient_thresh(self, val):
        self.bus.write_byte_data(self.addr, 0, val)

    def construct_sensor_array(self):
        for i in range(self.num_sensors):
            # sensors are 1-indexed for matching
            # silkscreen numbering J1, J2, ... etc.
            self.tof_arr.append(TOF(self.bus, i+1))

    def read_rb(self):
        self.bus.write_byte(self.addr, 253)
        block = self.bus.read_i2c_block_data(
            self.addr, 253, self.num_sensors*2)
        for i, tof in enumerate(self.tof_arr):
            tof.parse_rb(block[2*i:2*(i+1)])

    def read_distance(self):
        self.bus.write_byte(self.addr, 246)
        block = self.bus.read_i2c_block_data(
            self.addr, 246, self.num_sensors*2)
        for i, tof in enumerate(self.tof_arr):
            tof.parse_distance(block[2*i:2*(i+1)])

    def read_ambient(self):
        self.bus.write_byte(self.addr, 252)
        block = self.bus.read_i2c_block_data(
            self.addr, 252, self.num_sensors)
        for i, tof in enumerate(self.tof_arr):
            val = tof.parse_uint8_t(block[i])
            tof.ambient = val

    def read_signal(self):
        self.bus.write_byte(self.addr, 251)
        block = self.bus.read_i2c_block_data(
            self.addr, 251, self.num_sensors)
        for i, tof in enumerate(self.tof_arr):
            val = tof.parse_uint8_t(block[i])
            tof.signal = val

    def read_status(self):
        self.bus.write_byte(self.addr, 250)
        block = self.bus.read_i2c_block_data(
            self.addr, 250, self.num_sensors)
        for i, tof in enumerate(self.tof_arr):
            val = tof.parse_uint8_t(block[i])
            tof.status = val
        
    def read_num_spad(self):
        self.bus.write_byte(self.addr, 247)
        block = self.bus.read_i2c_block_data(
            self.addr, 247, self.num_sensors)
        for i, tof in enumerate(self.tof_arr):
            val = tof.parse_uint8_t(block[i])
            tof.num_spad = val

    def __str__(self):
        out_str = ""
        for i in range(self.num_sensors):
            out_str += str(self.tof_arr[i]) + '\n'
        return out_str


if __name__ == "__main__":
    rps = RPS()
    ambient_thresh = 255
    rps.write_ambient_thresh(ambient_thresh)
    while True:
        rps.read_rb()
        time.sleep(0.01)
        rps.read_ambient()
        time.sleep(0.01)
        rps.read_signal()
        time.sleep(0.01)
        rps.read_status()
        time.sleep(0.05)
        print(rps)
