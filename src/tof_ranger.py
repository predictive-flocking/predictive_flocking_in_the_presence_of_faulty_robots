#!/usr/bin/python
import rospy
import rel_pos_system as krps
from telloz.msg import range_n_bearing_sensor, range_n_robot_detector, tof, tof_arr
from std_msgs.msg import Float32


class rangeNbearing(object):

    def __init__(self, rps):
        self.rps = rps
        self.get_params()
        self.rb_pub = rospy.Publisher("sensors/range_n_bearing",
                                      range_n_bearing_sensor,
                                      queue_size=1)
        self.tof_arr_pub = rospy.Publisher("tof_arr",
                                      tof_arr,
                                      queue_size=1)
        self.tof_arr_real_pub = rospy.Publisher("tof_arr_real",
                                      tof_arr,
                                      queue_size=1)
    def parse_sensor_data(self):
        self.rps.read_rb()
        rb_list = []
        rb_msg = range_n_bearing_sensor()
        tof_arr_list= []
        for i in range(12):
            tof_msg = tof()
            tof_msg.range = self.rps.tof_arr[i].rb_distance
            # tof_msg.ambient = self.rps.tof_arr[i].ambient
            # tof_msg.signal = self.rps.tof_arr[i].signal
            # tof_msg.status = self.rps.tof_arr[i].status
            # tof_msg.num_spad = self.rps.tof_arr[i].num_spad
            tof_arr_list.append(tof_msg)
            rrd_msg = range_n_robot_detector()
            range_val = self.rps.tof_arr[i].rb_distance
            is_robot_val = self.rps.tof_arr[i].is_robot
            rrd_msg.range = range_val
            rrd_msg.is_robot = is_robot_val
            rb_list.append(rrd_msg)
        rb_msg = rb_list
        self.rb_pub.publish(rb_msg)

        tof_arr_msg = tof_arr()
        tof_arr_msg = tof_arr_list
        self.tof_arr_real_pub.publish(tof_arr_msg)


    def parse_all_data(self):
        # self.rps.read_distance()
        self.rps.read_ambient()
        self.rps.read_signal()
        # self.rps.read_status()
        # self.rps.read_num_spad()
        tof_arr_list= []
        for i in range(12):
            tof_msg = tof()
            # tof_msg.range = self.rps.tof_arr[i].distance
            tof_msg.ambient = self.rps.tof_arr[i].ambient
            tof_msg.signal = self.rps.tof_arr[i].signal
            # tof_msg.status = self.rps.tof_arr[i].status
            # tof_msg.num_spad = self.rps.tof_arr[i].num_spad
            tof_arr_list.append(tof_msg)
        tof_arr_msg = tof_arr()
        tof_arr_msg = tof_arr_list
        self.tof_arr_pub.publish(tof_arr_msg)

    def get_params(self):
        """
        params. are checked constantly and are
        updated
        """
        if rospy.has_param('/range_n_bearing'):
            rb_params = rospy.get_param('/range_n_bearing')
            self.rps.write_ambient_thresh(rb_params['ambient'])


def main():
    rospy.init_node('tof_node')
    try:
        rps = krps.RPS()
    except IOError:
        rospy.signal_shutdown("ToF Module is not available")
    rb = rangeNbearing(rps)
    freq = 20
    if rospy.has_param('/rb_freq'):
        freq = rospy.get_param('/rb_freq')
    rate = rospy.Rate(freq)
    while not rospy.is_shutdown():
        rb.parse_sensor_data()
        # rb.parse_all_data()
        rate.sleep()


if __name__ == '__main__':
    main()
