# Onboard Predictive Flocking of Quadcopter Swarm in the Presence of Obstacles and Faulty Robots



![Figure 1](g913.png)

The Python scripts used for the simulation and quadcopter experiments are located in ```src``` folder.

Scripts needed for controlling the Tello quadcopter:

```tello.py``` : Tello Python Developer Kit

```telloz_drive.py``` : Motion Control Utility Functions

```tello_commander.py``` : Motion Commander

Scripts needed for gathering data from range and bearing sensor:

```tof_ranger.py``` : Gathering Data from Range and Bearing via I2C

```rel_pos_system.py``` : Library File for I2C Commands

Script needed for Kalman-based estimations in swarms setting:

```ekf_emb.py``` : GNN and EKF Implementation for State Estimation of Neighboring Robots

Script for moving robots using the Predictive Search Model, including simulation and quadcopter experiment parameters:

```predictive_flocking.py``` : Predictive Search Model for Guiding Robot Swarm in the Presence of Obstacles and Faulty Robots

[Videos of the simulation and quadcopter experiments are available here.](https://www.youtube.com/playlist?list=PLY04vZs6xGr8U5Y8KWMD056ZRjaUVMh63)